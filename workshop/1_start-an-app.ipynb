{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "13ba06b9-5f2e-4de3-90da-511557166bfe",
   "metadata": {},
   "source": [
    "# Cloudify\n",
    "\n",
    "This notebook series guides you through the *cloudify* service: Serving Xarray datasets as zarr-datasets with xpublish and enabled server-side processing with dask. It introduces to the basic concepts with some examples. It was designed to work on DKRZ's HPC."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a56b764b-23a7-4d86-84b2-bf419d989cb2",
   "metadata": {},
   "source": [
    "## 1. Start an app\n",
    "\n",
    "In the following, you will learn how to start and control the cloudify service.\n",
    "\n",
    "**Is there any other reason why to run cloudify on the only internally accessible DKRZ HPC?**\n",
    "\n",
    "1. The Zarr-as-a-catalog functionality\n",
    "\n",
    "If you *cloudify* a virtual dataset prepared as a highly aggregated, analysis-ready dataset, clients can subset from this *one* large aggregated dataset instead of searching the file system.\n",
    "\n",
    "2. The data-as-a-service functionality\n",
    "\n",
    "In case you have less disk storage or if you are not sure if a user really needs to retrieve all the data of your specific product, you can just cloudify the workflow and create a virtual representation of the output. The product dataset is displayed *as if it was there* and whenever a chunk is retrieved, the workflow is triggered and the output is generated. This becomes useful when the workflow to generate the product is rather complex so that the users would struggle to execute it theirselves, e.g. if they do not have resources to run the workflow. Having said that, the data providers would spend some resources for the time the virtual dataset is hosted."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b17b450-716d-49b3-bbfa-aec943e47120",
   "metadata": {},
   "source": [
    "1. Install a kernel for jupyterhub\n",
    "\n",
    "```bash\n",
    "source activate /work/bm0021/conda-envs/cloudify\n",
    "python -m ipykernel install --user --name cloudify_env\n",
    "```\n",
    "\n",
    "-  Choose the kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cfb6129-aea7-4d87-a016-e04cee5bf084",
   "metadata": {},
   "source": [
    "2. For being able to allow secure *https* access, we need a ssl certificate. For testing purposes and for levante, we can use a self-signed one. Additionally, right now, some applications do only allow access through https. We can create it like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "0a748c3e-2a25-40ea-aefc-ae40bc13f664",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'l40356.lvt.dkrz.de'"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cn=!echo $HOSTNAME\n",
    "cn=cn[0]\n",
    "cn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d5e47e26-93ac-465f-90a4-8d84762b1f80",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "....+..+.......+..+.+.....+....+...+.....+.+............+.....+...+.+.....+..........+......+...+++++++++++++++++++++++++++++++++++++++++++++*.....+.+.....+....+..+.......+++++++++++++++++++++++++++++++++++++++++++++*...........+...+.+.....+.+.....+....+..+.......+..+..........+...+.......................+.............+.....+....+......+.....+....+...+........+.+.....+.........+.+........................+...........+...................+..+....+...+...+............+......+...........+.........+...............+......+.+............+.................................+.........+...+..+..................+.+..+....+...+.....+...+++++\n",
      "......+.....+.......+..+.........+.........+.+......+...+...........+.+...+.....+++++++++++++++++++++++++++++++++++++++++++++*.+.+...+......+...............+++++++++++++++++++++++++++++++++++++++++++++*....+.....+.+...+......+......+..+.......+...+.....+......+.......+...............+.....+............+...+....+...+...+...+...............+.....+.+.........+..+.........................+...+..+.........+.+...+..+.........+......+...+..........+.....+.+.....+...+............+...+...+...+....+...+..+............+....+.....+.......+...+..+............+......+..........+............+.........+........+..........+..+......+....+...........+...+....+...+.....+....+........+.......+........+......+.+...+..+.....................+....+..............+....+......+.........+..+...............+...+.+..+...................+.....+....+.....+.+...........+...............+...............+.......+...+.........+...+.......................+..........+..............+....+.....+.........+...+...............+...+.....................+....+.........+.....+...+....+......+...+............+.....+...+....+...........+.+...+..+.+.................+..........+..+......+...........................+.+.....................+......+..+...+..............................+....+...+........+.............+......+......+..+......+...............+.+........+......+.+.....+..................+.+...............+..+..........+.....+....+...+..................+...............+..+..........+........+...+....+..+....+.........+..+....+..............+...+...+......................+............+............+..............................+........+...+...+.+...+.....+...+++++\n",
      "-----\n"
     ]
    }
   ],
   "source": [
    "!openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 3650 -nodes -subj \"/C=XX/ST=Hamburg/L=Hamburg/O=Test/OU=Test/CN=\"{cn}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "190bde7f-6f36-4c87-a9f2-a82ee840302e",
   "metadata": {},
   "source": [
    "3. We write a cloudify script for data serving and start to host an example dataset in a background process.  We need to consider some settings:\n",
    "\n",
    "**Port**\n",
    "\n",
    "The resulting service listens on a specifc *port*. In case we share a node, we can only use ports that are not allocated already.  To enbale us all to run an own app, we agree to use a port `90XX` where XX are the last two digits of our account.\n",
    "\n",
    "**Dask Cluster**\n",
    "\n",
    "Dask is necessary for lazy access of the data. Additionally, a dask cluster can help us to do server-side processing like uniform encoding. When starting the imported predefined dask cluster, it will use the following resources:\n",
    "\n",
    "```python\n",
    "n_workers=2,\n",
    "threads_per_worker=8,\n",
    "memory_limit=\"16GB\"\n",
    "```\n",
    "\n",
    "which should be sufficient for at least two clients in parallel. We store it in an environment variable so that xpublish can find it. We futhermore have to allign the two event loops of dask and xpublish's asyncio with `nest_asyncio.apply()`. Event loops can be seen as *while* loops for a permanently running main worker.\n",
    "\n",
    "\n",
    "**Plug-ins**\n",
    "\n",
    "Xpublish finds pre-installed plugins like the intake-plugin by itself. Own plugins need to be registered.\n",
    "\n",
    "Further settings will be discussed later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e11d309f-c893-401a-ba5f-9f3f0046e039",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "xpublish_example_script=\"xpublish_example.py\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "571a82ea-d7bc-42e3-8169-ae22ef999065",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting xpublish_example.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile {xpublish_example_script}\n",
    "\n",
    "ssl_keyfile=\"/work/bm0021/k204210/cloudify/workshop/key.pem\"\n",
    "ssl_certfile=\"/work/bm0021/k204210/cloudify/workshop/cert.pem\"\n",
    "\n",
    "from cloudify.plugins.stacer import *\n",
    "from cloudify.plugins.geoanimation import *\n",
    "from cloudify.utils.daskhelper import *\n",
    "import xarray as xr\n",
    "import xpublish as xp\n",
    "import asyncio\n",
    "import nest_asyncio\n",
    "import sys\n",
    "import os\n",
    "def is_port_free(port, host=\"localhost\"):\n",
    "    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:\n",
    "        return s.connect_ex((host, port)) != 0  # Returns True if the port is free\n",
    "\n",
    "def find_free_port(start=5000, end=5100, host=\"localhost\"):\n",
    "    for port in range(start, end + 1):\n",
    "        if is_port_free(port, host):\n",
    "            return port\n",
    "    return None  # No free ports found\n",
    "\n",
    "port = find_free_port(9000,9100)\n",
    "if not port:\n",
    "    raise ValueError(\"Could not find a free port for service\")\n",
    "    \n",
    "nest_asyncio.apply()\n",
    "chunks={}\n",
    "for coord in [\"lon\",\"lat\"]:\n",
    "    chunk_size=os.environ.get(f\"XPUBLISH_{coord.upper()}_CHUNK_SIZE\",None)\n",
    "    if chunk_size:\n",
    "        chunks[coord]=int(chunk_size)\n",
    "\n",
    "l_lossy=os.environ.get(\"L_LOSSY\",False)\n",
    "\n",
    "def lossy_compress(partds):\n",
    "    import numcodecs\n",
    "    rounding = numcodecs.BitRound(keepbits=12)\n",
    "    return rounding.decode(rounding.encode(partds))\n",
    "\n",
    "if __name__ == \"__main__\":  # This avoids infinite subprocess creation\n",
    "    import dask\n",
    "    zarrcluster = asyncio.get_event_loop().run_until_complete(get_dask_cluster())\n",
    "    os.environ[\"ZARR_ADDRESS\"]=zarrcluster.scheduler._address\n",
    "    \n",
    "    dsname=sys.argv[1]\n",
    "    glob_inp=sys.argv[2:]\n",
    "\n",
    "    dsdict={}\n",
    "    ds=xr.open_mfdataset(\n",
    "        glob_inp,\n",
    "        compat=\"override\",\n",
    "        coords=\"minimal\",\n",
    "        chunks=chunks,\n",
    "    )\n",
    "    if \"height\" in ds:\n",
    "        del ds[\"height\"]\n",
    "    for dv in ds.variables:\n",
    "        if \"time\" in dv:\n",
    "            ds[dv]=ds[dv].load()\n",
    "            ds[dv].encoding[\"dtype\"] = \"float64\"\n",
    "            ds[dv].encoding[\"compressor\"] = None\n",
    "    ds=ds.set_coords([a for a in ds.data_vars if \"bnds\" in a])\n",
    "    if l_lossy:\n",
    "        ds = xr.apply_ufunc(\n",
    "            lossy_compress,\n",
    "            ds,\n",
    "            dask=\"parallelized\", \n",
    "            keep_attrs=\"drop_conflicts\"\n",
    "        )\n",
    "    dsdict[dsname]=ds\n",
    "    \n",
    "    collection = xp.Rest(dsdict)\n",
    "    collection.register_plugin(Stac())\n",
    "    collection.register_plugin(PlotPlugin())\n",
    "    collection.serve(\n",
    "        host=\"0.0.0.0\",\n",
    "        port=port,\n",
    "        ssl_keyfile=ssl_keyfile,\n",
    "        ssl_certfile=ssl_certfile\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca64c11f-0846-4ddd-9e60-4b22dba8b32c",
   "metadata": {},
   "source": [
    "You can run this app e.g. for:\n",
    "```\n",
    "dsname=\"example\"\n",
    "glob_inp=\"/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/*.nc\"\n",
    "```\n",
    "by applying:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5da13d6b-05f1-4b3b-aecd-1ac3bb635526",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%bash --bg\n",
    "#Cannot use variables from python script here so it is all hard-coded\n",
    "\n",
    "source activate /work/bm0021/conda-envs/cloudify\n",
    "python xpublish_example.py \\\n",
    "    example \\\n",
    "    /work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/*.nc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "634d1952-43a9-40a7-b7c3-9bbff5f07081",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Stop a running app\n",
    "\n",
    "Let us try to just run **one** app at the time. Otherwise, we would have multiple ports and dask clusters. It wouldnt end up well.\n",
    "\n",
    "You can check for the main *cloudify* processes by finding the dask workers. In a next step, you can *kill* by ID."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9a43c4ce-be08-4493-8dd5-a3789f8c0647",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "k204210  1885374 1878744  0 09:31 ?        00:00:00 /work/bm0021/conda-envs/cloudify/bin/python -Xfrozen_modules=off -m ipykernel_launcher -f /home/k/k204210/.local/share/jupyter/runtime/kernel-449f1aed-0c01-4339-8e8e-3391add9c830.json\n",
      "k204210  1885397 1878744  0 09:31 ?        00:00:00 /work/bm0021/conda-envs/cloudify/bin/python -Xfrozen_modules=off -m ipykernel_launcher -f /home/k/k204210/.local/share/jupyter/runtime/kernel-59fedbd8-7d7f-424a-8226-3980eadf7fc6.json\n",
      "k204210  1886037 1878744  1 09:35 ?        00:00:21 /work/bm0021/conda-envs/cloudify/bin/python -Xfrozen_modules=off -m ipykernel_launcher -f /home/k/k204210/.local/share/jupyter/runtime/kernel-1a921a3d-65a6-4b07-9c29-d17f990ab11b.json\n",
      "k204210  1894380 1878744 17 09:58 ?        00:00:00 /work/bm0021/conda-envs/cloudify/bin/python -Xfrozen_modules=off -m ipykernel_launcher -f /home/k/k204210/.local/share/jupyter/runtime/kernel-1d363456-8c8d-40f5-91ea-1e931e364b72.json\n",
      "k204210  1894407 1894380  0 09:58 pts/3    00:00:00 /bin/bash -c ps -ef | grep cloudify\n",
      "k204210  1894411 1894410  0 09:58 ?        00:00:00 /sw/spack-levante/jupyterhub/jupyterhub/bin/python /sw/spack-levante/jupyterhub/jupyterhub/bin/conda shell.posix activate /work/bm0021/conda-envs/cloudify\n",
      "k204210  1894413 1894407  0 09:58 pts/3    00:00:00 grep cloudify\n"
     ]
    }
   ],
   "source": [
    "!ps -ef | grep cloudify"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b505b0a-2b48-4eb3-8c91-fb6b7fcdc54b",
   "metadata": {
    "tags": []
   },
   "source": [
    "**Important note:**\n",
    "\n",
    "If you plan to continue with another notebook, do not stop the app now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "af33c134-f4ba-42f7-9687-7bb9948d5dfe",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/bin/bash: line 0: kill: (1882536) - No such process\n"
     ]
    }
   ],
   "source": [
    "!kill 1882536"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "feacaed0-df8d-4e52-af8c-acd094cac6f4",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cloudify",
   "language": "python",
   "name": "cloudify"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

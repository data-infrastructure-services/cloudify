1. Open a terminal, `source activate /work/bm0021/conda-envs/cloudify`
2. Change into this directory, modify and run `host.py`
3. Open another terminal, change into this directory.
4. Get swift tokens for bb1364 and store them in a `mytoken.py`file
5. module load python3/unstable
6. Modify and run `generate_plots_into_swift.py`
7. Modify and run `generate_table_app.py`
8. Run `mime.sh`